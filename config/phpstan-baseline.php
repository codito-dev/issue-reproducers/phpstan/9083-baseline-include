<?php declare(strict_types = 1);

$ignoreErrors = [];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property Configurator\\:\\:\\$foo\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/Configurator.php',
];
$ignoreErrors[] = [
	'message' => '#^Undefined variable\\: \\$bar$#',
	'count' => 1,
	'path' => __DIR__ . '/Configurator.php',
];

return ['parameters' => ['ignoreErrors' => $ignoreErrors]];
