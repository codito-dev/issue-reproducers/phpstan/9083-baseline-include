<?php declare(strict_types = 1);

$ignoreErrors = [];
$ignoreErrors[] = [
	'message' => '#^Method App\\\\Foo\\\\FooFactory\\:\\:__construct\\(\\) has parameter \\$foo with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/Foo/FooFactory.php',
];
$ignoreErrors[] = [
	'message' => '#^Property App\\\\Foo\\\\FooFactory\\:\\:\\$foo has no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/Foo/FooFactory.php',
];
$ignoreErrors[] = [
	'message' => '#^Property App\\\\Foo\\\\FooFactory\\:\\:\\$foo is never read, only written\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/Foo/FooFactory.php',
];

return ['parameters' => ['ignoreErrors' => $ignoreErrors]];
