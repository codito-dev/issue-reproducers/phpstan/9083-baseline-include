<?php

declare(strict_types=1);

namespace App\Command;

use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SplitPhpStanBaselineCommand extends Command
{
    protected static $defaultName = 'phpstan:split-baseline';
    protected static $defaultDescription = 'Splits full PHPStan baseline into area-scoped files';

    private string $rootPath;

    public function __construct(string $rootPath)
    {
        $this->rootPath = $rootPath;

        parent::__construct(self::$defaultName);
    }

    protected function configure(): void
    {
        $this
            ->setDefinition([
                new InputArgument(
                    'full-baseline-path',
                    InputArgument::OPTIONAL,
                    'Relative path to full baseline',
                    'utils/phpstan/baseline-full.php'
                ),
            ]);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $baselinePath = realpath($this->rootPath . '/' . $input->getArgument('full-baseline-path'));

        if (false === $baselinePath) {
            throw new RuntimeException("Provided baseline path is invalid.");
        }

        $baseline = require $baselinePath;
        $errors = $baseline['parameters']['ignoreErrors'] ?? [];
        $areas = [];

        foreach ($errors as $error) {
            $relativePath = (string) substr((string) realpath($error['path']), strlen($this->rootPath) + 1);
            preg_match('#^((packages/[a-zA-Z0-9])+|[^/]+)+#', $relativePath, $matches);
            $outputDir = $matches[0];

            $error['path'] = $relativePath;
            $areas[$outputDir][] = $error;
        }

        foreach ($areas as $area => $areaErrors) {
            // See: https://github.com/phpstan/phpstan-src/blob/1.10.x/src/Command/ErrorFormatter/BaselinePhpErrorFormatter.php
            $php = '<?php declare(strict_types = 1);';
            $php .= "\n\n";
            $php .= '$ignoreErrors = [];';
            $php .= "\n";

            foreach ($areaErrors as $error) {
                $php .= sprintf(
                    "\$ignoreErrors[] = [\n\t'message' => %s,\n\t'count' => %d,\n\t'path' => __DIR__ . %s,\n];\n",
                    var_export($error['message'], true),
                    var_export($error['count'], true),
                    var_export('/' . substr($error['path'], strlen($area) + 1), true),
                );
            }

            $php .= "\n";
            $php .= 'return [\'parameters\' => [\'ignoreErrors\' => $ignoreErrors]];';
            $php .= "\n";

            file_put_contents($area . '/phpstan-baseline.php', $php);

            $output->writeln("[$area] OK");
        }

        return Command::SUCCESS;
    }
}
