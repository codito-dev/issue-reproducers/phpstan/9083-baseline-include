<?php

declare(strict_types=1);

namespace App\Foo;

final class FooFactory
{
    private $foo;

    public function __construct($foo)
    {
        $this->foo = $foo;
    }
}
