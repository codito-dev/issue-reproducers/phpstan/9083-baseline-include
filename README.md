# PHPStan baseline self-handling

This little repository reproduces problem asked [here](https://github.com/phpstan/phpstan/discussions/9083).

- Running `phpstan:baseline` does not take existing baselines into consideration and
  dumped `tools/phpstan/baseline-full.php` does not contain already dumped rules from `config/phpstan-baseline.php`.
  Running `composer phpstan` after it will report errors that previously were ignored.
- Running `phpstan:baseline:no-include` dumps baseline without current baseline context (does not include existing
  rules) and collected rules are complete: running analysis after it will return 0 errors.

## Context

We have large application (2M+ SLOC, 28k+ files covered by PHPStan) which is a modular monolith. There are several dev teams that work on their areas of code. Since PHPStan was introduced on existing app, large amount of errors was dumped to baseline files. Yes, _files_ - we use multiple files because each team wants to work with their own set of errors to fix. Having all errors in one baseline file would introduce problems:

- performance issues of an IDE (single baseline file is ~14MB)
- risk of merge conflicts between branches that introduced/fixed errors to/from baseline

That's why we have several area-scoped baseline files, like here in the repo. We also prepared command, also shown in this repo, that generates baseline in full-project context and then splits it into area-scoped baseline files.
