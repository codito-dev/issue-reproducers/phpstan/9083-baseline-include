<?php declare(strict_types = 1);

$ignoreErrors = [];
$ignoreErrors[] = [
	'message' => '#^Instanceof between App\\\\Kernel and App\\\\Kernel will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/console',
];

return ['parameters' => ['ignoreErrors' => $ignoreErrors]];
